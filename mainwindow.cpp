#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include "QWidget"
#include <QMainWindow>
#include <QDebug>
#include <sstream>

#include "qtextadapter.h"
#include "qdateadapter.h"
#include "qtimeadapter.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(new QWidget());
    _layout = new QVBoxLayout();
    centralWidget()->setLayout(_layout);
    //text adapter check
    _text = new QTextEdit();
    _layout->addWidget(_text);
    QTextAdapter *textAdapter = new QTextAdapter(_text);
    std::string bla = "bla";
    textAdapter->setText(bla);
    textAdapter->append(bla + bla);
    std::cout << "=Text adapter check=\ntoPlainText():\n" << textAdapter->toPlainText() << std::endl;
    //date adapter check
    time_t t = time(0); //returns current date&time
    QDateAdapter *dateAdapter = new QDateAdapter(t);
    _date = dateAdapter->getAdaptee();
    _layout->addWidget(_date);
    qDebug() << "=Date adapter check=\nDate after constructor:\n"
             << _date->date().toString();
    dateAdapter->setDate(t*2);
    qDebug() << "setDate(t*2):\n" << _date->date().toString();
    std::cout << "date():\n" << dateAdapter->date() << std::endl;
    //time adapter check
    QTimeAdapter *timeAdapter = new QTimeAdapter(time(0));
    _time = timeAdapter->getAdaptee();
    _layout->addWidget(_time);
    qDebug() << "=TIme adapter check=\nTime after construtor:\n"
             << _time->time().toString();
    timeAdapter->setTime(time(0)*2);
    qDebug() << "setTime(t*2):\n" << _time->time().toString();
    qDebug() << "time():\n" << timeAdapter->time();
    qDebug() << "<< Done >>\n";
}

MainWindow::~MainWindow()
{
    delete ui;
}













































QString lyrics = QString("It's just a cigarette and it cannot be that bad\n") +
QString("Honey don't you love me and you know it makes me sad?\n") +
QString("It's just a cigarette like you always used to do\n") +
QString("I was different then, I don't need them to be cool\n") +
QString("It's just a cigarette and it harms your pretty lungs\n") +
QString("Well it's only twice a week so there's not much of a chance\n") +
QString("It's just a cigarette it'll soon be only ten\n") +
QString("Honey can't you trust me when I want to stop I can\n") +
QString("It's just a cigarette and it's just a Malboro Light\n") +
QString("Maybe but is it worth it if we fight?\n") +
QString("It's just a cigarette that I got from Jamie-Lee\n") +
QString("She's gonna get a smack and I'm gonna give you three\n") +
QString("It's just a cigarette and I only did it once (It's just a cigarette it'll soon be only ten)") +
QString("It's only twice a week so there's not much of a chance (It'll make you sick girl there's not much of a chance)") +
QString("It's just a cigarette and I'm sorry that I did it (It's just a cigarette you'll be sorry that you did it)") +
QString("Honey can't you trust me when I want to stop I can");
