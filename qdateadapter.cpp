#include "qdateadapter.h"
#include "QDateTime"
#include "QDate"
#include "QDateEdit"

QDateAdapter::QDateAdapter(QDateEdit *p)
{
    _adaptee = p;
}

QDateAdapter::QDateAdapter(time_t time) {
    _adaptee = new QDateEdit();
    QDateTime draft;
    draft.setTime_t(time);
    _adaptee->setDate(draft.date());
}

void QDateAdapter::setDate(time_t time) {
    QDateTime draft;
    draft.setTime_t(time);
    _adaptee->setDate(draft.date());
    return;
}

time_t QDateAdapter::date() {
    QDateTime draft = _adaptee->date().startOfDay();
    return draft.toTime_t();
}

QDateEdit* QDateAdapter::getAdaptee() {
    return _adaptee;
}

void QDateAdapter::bind(QDateEdit *p) {
    _adaptee = p;
    return;
}


