#ifndef QDATEADAPTER_H
#define QDATEADAPTER_H
#include "QDateEdit"
#include "QDateTime"


class QDateAdapter : public QDateEdit
{
private:
    QDateEdit *_adaptee;
public:
    QDateAdapter(QDateEdit *p);
    QDateAdapter(time_t time);
    void setDate(time_t time);
    time_t date();

    void bind(QDateEdit *p);
    QDateEdit* getAdaptee();
};

#endif // QDATEADAPTER_H
