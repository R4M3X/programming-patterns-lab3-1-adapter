#include "qtextadapter.h"
#include <QTextEdit>
#include <QString>

QTextAdapter::QTextAdapter(QTextEdit *p)
{
    _adaptee = p;
}

void QTextAdapter::append(const std::string str) {
    _adaptee->append( QString::fromStdString(str) );
    return;
}

void QTextAdapter::setText(const std::string str) {
    _adaptee->setText( QString::fromStdString(str) );
    return;
}

std::string QTextAdapter::toPlainText() {
    return _adaptee->toPlainText().toStdString();
}
