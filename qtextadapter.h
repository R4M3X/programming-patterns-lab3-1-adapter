#ifndef QTEXTADAPTER_H
#define QTEXTADAPTER_H
#include <QTextEdit>


class QTextAdapter : public QTextEdit
{
private:
    QTextEdit *_adaptee;
public:
    QTextAdapter(QTextEdit *p);
    void append(const std::string str);
    void setText(const std::string str);
    std::string toPlainText();
};

#endif // QTEXTADAPTER_H
