#include "qtimeadapter.h"

QTimeAdapter::QTimeAdapter(time_t t)
{
    _adaptee = new QTimeEdit();
    this->setTime(t);
}

void QTimeAdapter::setTime(time_t t) {
    QDateTime draft;
    draft.setTime_t(t);
    _adaptee->setTime(draft.time());
}

time_t QTimeAdapter::time() {
    return _adaptee->dateTime().toTime_t();
}
QTimeEdit* QTimeAdapter::getAdaptee() {
    return _adaptee;
}
