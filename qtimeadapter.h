#ifndef QTIMEADAPTER_H
#define QTIMEADAPTER_H
#include <QTimeEdit>

class QTimeAdapter : public QTimeEdit
{
private:
    QTimeEdit* _adaptee;
public:
    QTimeAdapter(time_t t);
    void setTime(time_t t);
    time_t time();
    QTimeEdit* getAdaptee();
};

#endif // QTIMEADAPTER_H
